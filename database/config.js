require("dotenv").config();
const MongoClient = require("mongodb").MongoClient;
const MONGO_URI = "mongodb://localhost:27017";
exports.connection = () => {
  return new Promise(async (resolve, reject) => {
    await MongoClient.connect(
      MONGO_URI,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err, client) => {
        if (err) console.log("error", err);
        const db = client.db("sattv");
        console.log("Connection is okay");
        resolve(db);
      }
    );
  });
};
