const db = require("../server");
let User = db.collection("users");
let BasePacks = db.collection("basepacks");
let Channels = db.collection("channels");
let Services = db.collection("services");
let Subscriptions = db.collection("subscriptions");
let query = require("../query/query");
const { ObjectId } = require("mongodb");
const sendMail = require("../helper/sendmail");

require("dotenv").config();

// To insert Dummy data
// Data inserted firstname,lastname,email,phonenumber,balance,basepacks
const signup = async (req, res, next) => {
  let data = req.query;
  console.log("data", data);
  const user = await query.insert(User, data);

  res.json({
    message: "Data inserted successfully",
    data: user.ops[0],
  });
};

// View current balance in the account
const viewBalance = async (req, res, next) => {
  let user = await query.find(User, { email: req.query.email });

  res.json({
    userName: user[0].email,
    curentBalance: user[0].balance,
  });
};

// Recharge completed successfully
const updateBalance = async (req, res, next) => {
  let balance = parseInt(req.query.balance);

  let user = await query.find(User, { email: req.query.email });

  let userBalance = parseInt(user[0].balance);
  let totalBalance = userBalance + balance;
  let result = await query.findOneAndUpdate(
    User,
    { email: req.query.email },
    {
      $set: { balance: totalBalance },
    },
    { returnOriginal: false }
  );

  res.json({
    message: "Balance Updated Successfully",
    updatedBalance: result.value.balance,
  });
};

//View available packs, channels and services
const viewpacksandServices = async (req, res, next) => {
  let basePacks = await query.find(BasePacks, {});

  let channels = await query.find(Channels, {});

  let services = await query.find(Services, {});

  res.json({
    basePacks: basePacks,
    channels: channels,
    services: services,
  });
};

//Subscribe to channel packs
const createSubscriptions = async (req, res, next) => {
  let data = req.query;

  const user = await query.find(User, { email: req.query.email });

  let UserId = ObjectId(user[0]._id);
  let balance = user[0].balance;

  data.userId = UserId;
  data.email = req.query.email;

  const subresponse = await query.insert(Subscriptions, data);
  console.log("subresponse", subresponse.ops[0].months);
  if (subresponse.ops[0].months == 3) {
    let monthly_price = parseInt(subresponse.ops[0].price);

    let subscriptionAmount = monthly_price * subresponse.ops[0].months;

    const discount = 10;
    amountDiscounted = parseInt(subscriptionAmount / discount);

    let deduction = subscriptionAmount - amountDiscounted;
    let actualAmount = balance - deduction;
    console.log("actualAmount", actualAmount);
    let result = await query.findOneAndUpdate(
      User,
      { email: req.query.email },
      {
        $set: { balance: actualAmount },
      },
      { returnOriginal: false }
    );
    let type = subresponse.ops[0].type;
    let emailId = req.query.email;
    let subject = `You have successfully subscribed the following packs - ${type}`;
    let body = {
      SubscriptionAmount: subscriptionAmount,
      Discountapplied: amountDiscounted,
      FinalPriceafterdiscount: deduction,
      Accountbalance: actualAmount,
    };
    // to send email
    await sendMail(emailId, subject, body);
    res.json({
      subscriptionInfo: subresponse.ops[0],
      balanceLeft: result.value.balance,
    });
  } else {
    res.json({
      subscriptionInfo: subresponse.ops[0],
    });
  }
};

//Subscribe to special services
const subscribeServices = async (req, res, next) => {
  let service = await query.find(Services, {
    serviceName: req.query.servicename,
  });
  console.log("service", service[0].price);
  let price = service[0].price;
  let user = await query.find(User, { email: req.query.email });

  let userBalance = parseInt(user[0].balance);
  let actualBalance = userBalance - price;
  let result = await query.findOneAndUpdate(
    User,
    { email: req.query.email },
    {
      $set: { balance: actualBalance },
    },
    { returnOriginal: false }
  );
  let emailId = req.query.email;
  let subject = `You have successfully subscribed to following service - ${service[0].serviceName}`;
  let body = {
    SubscriptionAmount: price,
    Accountbalance: actualBalance,
  };
  await sendMail(emailId, subject, body);
  res.json({
    message: "Subscribe to service ",
    updatedBalance: result.value.balance,
  });
};

//Update email and phone number for notifications
const updateEmailandnumber = async (req, res, next) => {
  let result = await query.findOneAndUpdate(
    User,
    { email: req.query.email },
    {
      $set: {
        email: req.query.email,
        phonenumber: req.query.number,
      },
    },
    { returnOriginal: false }
  );

  res.json({
    message: "Updated Successfully",
    data: result.value,
  });
};
module.exports = {
  signup,
  viewBalance,
  updateBalance,
  viewpacksandServices,
  createSubscriptions,
  subscribeServices,
  updateEmailandnumber,
};
