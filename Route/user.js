const router = require("express").Router();
const userCrtl = require("../controller/user");

// To insert dummy data
router.post("/signup", userCrtl.signup);

// To viewBalance
router.get("/viewBalance", userCrtl.viewBalance);

// To updateBalance
router.put("/updateBalance", userCrtl.updateBalance);

// To viewpacksandServices
router.get("/viewpacksandServices", userCrtl.viewpacksandServices);

// To createSubscriptions
router.post("/createSubscriptions", userCrtl.createSubscriptions);

// To subscribeServices
router.post("/subscribeServices", userCrtl.subscribeServices);

// To updateEmailandnumber
router.put("/updateEmailandnumber", userCrtl.updateEmailandnumber);

module.exports = router;
